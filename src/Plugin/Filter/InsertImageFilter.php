<?php

namespace Drupal\insert_image\Plugin\Filter;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\file\Plugin\Field\FieldType\FileFieldItemList;
use Drupal\filter\FilterProcessResult;
use Drupal\filter\Plugin\FilterBase;

/**
 * Text filter that allows to use (pseudo-) tokens to insert inline images.
 *
 * Allows to use pseudo-tokens [image:DELTA:STYLE] that are to be replaced by
 * actual themed images when entity is being viewed.
 *
 * @Filter(
 *   id = "insert_image_filter",
 *   title = @Translation("Insert Images"),
 *   description = @Translation("Replaces pseudo-tokens [image:DELTA:STYLE] with themed images."),
 *   type = Drupal\filter\Plugin\FilterInterface::TYPE_MARKUP_LANGUAGE,
 * )
 */
class InsertImageFilter extends FilterBase {

  /**
   * {@inheritdoc}
   *
   * What we do here, is searching for [image:DELTA:STYLE] pseudo-tokens in the
   * $text and replacing theme with actual themed images.
   */
  public function process($text, $langcode) {

    // Source images are stored in the field specified in filter settings.
    if (!empty($this->settings['field_name'])) {
      $field_name = $this->settings['field_name'];

      // A trick to get view mode and entity being viewed, although current
      // class and method have no info about them.
      // @see insert_image_preprocess_field().
      $entity = drupal_static('insert_image_entity', NULL);
      $view_mode = drupal_static('insert_image_view_mode', NULL);

      // One more silly check...
      if (!empty($entity) && $entity instanceof ContentEntityInterface && $entity->hasField($field_name)) {
        $field = $entity->{$field_name};

        // One more silly check...
        if ($field instanceof FileFieldItemList && !$field->isEmpty()) {

          // We allow 2 pseudo-token patterns: [image:DELTA] and
          // [image:DELTA:STYLE]. In the first case default (unstyled) image
          // will be used.
          $pattern = '@\[image:(\d+)(:([\w\d_]+))?\]@';

          // Do process text.
          $text = preg_replace_callback($pattern, function ($matches) use ($entity, $view_mode, $field) {
            $delta = $matches[1];
            $style = empty($matches[3]) ? NULL : $matches[3];

            /** @var \Drupal\Core\Field\FieldItemInterface $field_item */
            if ($field_item = $field->get($delta)) {
              /** @var \Drupal\file\Entity\File $image */
              $image = $field_item->entity;

              return \Drupal::theme()->render('insert_image', [
                'view_mode' => $view_mode,
                'entity' => $entity,
                'image' => $image,
                'style' => $style,
              ]);
            }
            return NULL;
          }, $text);
        }
      }
    }

    return new FilterProcessResult($text);
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $form['field_name'] = array(
      '#type' => 'machine_name',
      '#title' => $this->t('Field name'),
      '#default_value' => empty($this->settings['field_name']) ? 'field_inline_images' : $this->settings['field_name'],
      '#description' => $this->t('Inline images field name.'),
      '#required' => FALSE,
    );
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function tips($long = FALSE) {
    if ($field_name = $this->settings['field_name']) {
      if ($long) {
        return $this->t('
          <p>Tokens <code>[image:DELTA:STYLE]</code> to be replaced by images from %field_name field. 
          Here <code>DELTA</code> is the index of the image in %field_name field you want to insert,
          and <code>STYLE</code> is the machine name of the image style (<em>thumbnail</em>, <em>medium</em>, 
          <em>large</em> etc.).</p>
          <p>Here is an example: <code>[image:1:large]</code>. This code will be replaced by the 
          <em>second</em> image from %field_name field, using <em>large</em> image style.</p>
          <p>You may omit image style: <code>[image:DELTA]</code>. In this case, default image will be used.</p>', [
            '%field_name' => $field_name,
          ]);
      }
      else {
        return $this->t('Tokens <code>[image:DELTA:STYLE]</code> to be replaced by images from %field_name field.', [
          '%field_name' => $field_name,
        ]);
      }
    }
  }

}
