<?php

/**
 * @file
 * Allows to use [image:DELTA] token to insert images into text.
 */

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\image\Entity\ImageStyle;

/**
 * Implements hook_preprocess_field().
 */
function insert_image_preprocess_field(&$variables) {

  // A trick to get an entity being viewed from FilterBase's process() method.
  // @see \Drupal\insert_image\Plugin\Filter\InsertImageFilter::process()
  $element = &$variables['element'];
  if (isset($element['#object']) && $element['#object'] instanceof ContentEntityInterface) {
    $entity = &drupal_static('insert_image_entity');
    $entity = $element['#object'];
  }

  // The same for view mode. It may be useful for future image styling.
  if (isset($element['#view_mode'])) {
    $view_mode = &drupal_static('insert_image_view_mode');
    $view_mode = $element['#view_mode'];
  }
}

/**
 * Implements hook_theme().
 */
function insert_image_theme($existing, $type, $theme, $path) {
  return [
    'insert_image' => [
      'variables' => [
        'view_mode' => NULL,
        'entity' => NULL,
        'image' => NULL,
        'style' => NULL,
      ],
    ],
  ];
}

/**
 * Implements hook_theme_suggestions_HOOK().
 */
function insert_image_theme_suggestions_insert_image(array $variables) {
  $suggestions = [];

  $entity = $variables['entity'];
  if ($entity instanceof EntityInterface) {
    $entity_type = $entity->getEntityTypeId();
    $entity_bundle = $entity->bundle();
    $view_mode = strtr($variables['view_mode'], '.', '_');
    // Allow to vary templates per entity type, bundle and view mode. For
    // example, use insert-image--node--article--teaser.html.twig template to
    // style inserted images for nodes of type article being viewed in teaser
    // mode.
    $suggestions[] = 'insert_image__' . $entity_type;
    $suggestions[] = 'insert_image__' . $entity_type . '__' . $entity_bundle;
    $suggestions[] = 'insert_image__' . $entity_type . '__' . $entity_bundle . '__' . $view_mode;
  }

  return $suggestions;
}

/**
 * Prepares variables for insert image templates.
 */
function template_preprocess_insert_image(&$variables) {
  /** @var \Drupal\file\Entity\File $image */
  $image = $variables['image'];
  $style_name = $variables['style'];

  // If image style is set, use it for generation image URL.
  if ($style_name) {
    /** @var \Drupal\image\ImageStyleInterface $style */
    if ($style = ImageStyle::load($style_name)) {
      $image_url = $style->buildUrl($image->getFileUri());
    }
  }

  // If image style is not set, fallback to the default image URL.
  if (empty($image_url)) {
    $image_url = $image->url();
  }

  // Now $variables['url'] contains absolute URL. If you need relative URL,
  // implement THEME_preprocess_insert_image() and use following code:
  //
  //   $variables['url'] = file_url_transform_relative($variables['url']);
  //
  // This will replace absolute image url with relative one.
  $variables['url'] = $image_url;
}
