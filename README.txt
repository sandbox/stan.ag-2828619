CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration


INTRODUCTION
------------

The Insert Image module is a kind of replacement of Insert module
for Drupal 8. It allows you to manually insert images right into the
text fields.

Unlike Insert, this module obliges you to use pseudo-tokens like
[image:DELTA:STYLE], that will be replaced with themed images when
viewing the entity. Here DELTA is the index (starting from 0) of the
image being inserted, and STYLE is the name of image style
(STYLE may be omitted; in this case default image will be shown).
For example, here is a valid token: [image:0:thumbnail]

 * For a full description of the module, visit the project page:
   https://www.drupal.org/sandbox/stanag/2828619

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/2828619


REQUIREMENTS
------------

No special requirements.


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-8
   for further information.


CONFIGURATION
-------------

Technically saying, this module provides a text filter, which should be
configured before using:

 * After installation, go to http://YOURSITE/admin/config/content/formats
   and click "Configure" for text format you want to set up.

 * On the text format configure screen, under "Enabled filters" section,
   check "Insert Images" option.

 * Below, under "Filter settings" section, choose "Insert Images" tab and
   specify name of the field that contains images you want to insert
   (for example, field_inline_images).
